const redis = require("redis");
const util = require("util");

const REDIS_COMMANDS = ["get", "set", "del", "keys"];

const redisKey = (key, clientKey) => {
  return clientKey ? `${clientKey}:${key}` : key;
};

const installationKey = forgeInstallationId => {
  return `installation:${forgeInstallationId};`;
};

class RedisAdapter {
  constructor(logger, opts) {
    const redisClient = redis.createClient(process.env["DB_URL"] || opts.url);

    this.client = REDIS_COMMANDS.reduce((client, command) => {
      client[command] = util.promisify(redisClient[command]).bind(redisClient);
      return client;
    }, {});
  }

  async get(key, clientKey) {
    const val = await this.client.get(redisKey(key, clientKey));

    try {
      return JSON.parse(val);
    } catch (e) {
      return val;
    }
  }

  async saveInstallation(val, clientKey) {
    const clientSetting = await this.set("clientInfo", val, clientKey);

    const forgeInstallationId = clientSetting.installationId;
    if (forgeInstallationId) {
      await this.associateInstallations(forgeInstallationId, clientKey);
    }

    return clientSetting;
  }

  async set(key, val, clientKey) {
    let strVal = val;

    if (typeof val !== "string") {
      strVal = JSON.stringify(val);
    }

    await this.client.set(redisKey(key, clientKey), strVal);
    return this.get(key, clientKey);
  }

  async del(key, clientKey) {
    await this.client.del(redisKey(key, clientKey));
  }

  async getAllClientInfos() {
    const keys = await this.client.keys("*:clientInfo");

    return Promise.all(
      keys.map(key => {
        return this.get(key);
      })
    );
  }

  isMemoryStore() {
    return false;
  }

  async associateInstallations(forgeInstallationId, clientKey) {
    await this.client.set(installationKey(forgeInstallationId), clientKey);
  }

  async deleteAssociation(forgeInstallationId) {
    await this.client.del(installationKey(forgeInstallationId));
  }

  async getClientSettingsForForgeInstallation(forgeInstallationId) {
    const clientKey = await this.client.get(
      installationKey(forgeInstallationId)
    );
    if (!clientKey) {
      return null;
    }
    return this.get("clientInfo", clientKey);
  }
}

module.exports = function (logger, opts) {
  if (arguments.length === 0) {
    return RedisAdapter;
  }

  return new RedisAdapter(logger, opts);
};
