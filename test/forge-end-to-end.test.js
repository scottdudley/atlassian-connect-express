const helper = require("./test_helper");
const {
  mockForgeJwksServer,
  startConnectServer,
  installDummyTenant,
  forgeInvocationSampleData,
  mockInstallKeyServer
} = require("./forge-end-to-end_helper");
const request = require("request");
const nock = require("nock");

function createTestContext() {
  const testContext = {};

  async function stopServer() {
    if (testContext.server) {
      testContext.server.close();
    }
    nock.cleanAll();
  }

  async function startServer(install, appId, forgeInstallationId) {
    await stopServer();
    mockInstallKeyServer();
    testContext.jwt = await mockForgeJwksServer({
      app: {
        installationId: forgeInstallationId
      }
    });
    const connectServer = await startConnectServer({
      storeKey: "forgewithconnect",
      port: helper.addonPort,
      localBaseUrl: helper.addonBaseUrl,
      appId
    });
    if (install) {
      await installDummyTenant(forgeInstallationId);
    }
    testContext.addon = connectServer.addon;
    testContext.app = connectServer.app;
    testContext.server = connectServer.server;
  }

  async function makeAppRequest(method, path, headers) {
    return new Promise((resolve, reject) => {
      request(
        `${helper.addonBaseUrl}${path}`,
        { method, headers },
        (error, response, body) => {
          if (error) {
            reject(error);
          } else {
            resolve({ response, body });
          }
        }
      );
    });
  }

  return { startServer, stopServer, makeAppRequest, testContext };
}

describe("Forge remote requests with a backing Connect Installation", () => {
  const { startServer, stopServer, makeAppRequest, testContext } =
    createTestContext();
  beforeEach(() =>
    startServer(
      true,
      forgeInvocationSampleData.app.id,
      helper.forgeInstallationId
    )
  );
  afterAll(stopServer);
  it("Proceeds when no associated Connect data is found", async () => {
    await startServer(
      false,
      forgeInvocationSampleData.app.id,
      helper.forgeInstallationId
    );
    const path = "/associateConnect";
    testContext.app.get(
      path,
      [
        testContext.addon.authenticateForge(),
        testContext.addon.associateConnect()
      ],
      async (req, res) => {
        res.end("Made it to the request handler");
      }
    );
    const response = await makeAppRequest("GET", path, {
      authorization: `bearer ${testContext.jwt}`,
      "x-forge-oauth-system": "foo",
      "x-forge-oauth-user": "bar"
    });
    expect(response.body).toContain("Made it to the request handler");
  });
  it("Retrieves data from the associated stored Connect data", async () => {
    const appToken = "app-token";
    const userToken = "user-token";
    const expectedForgeContext = {
      tokens: { app: appToken, user: userToken },
      app: {
        id: forgeInvocationSampleData.app.id,
        apiBaseUrl: forgeInvocationSampleData.app.apiBaseUrl,
        installationId: helper.forgeInstallationId
      },
      context: {
        cloudId: forgeInvocationSampleData.context.cloudId,
        siteUrl: forgeInvocationSampleData.context.siteUrl
      },
      apiBaseUrl: forgeInvocationSampleData.app.apiBaseUrl,
      principal: forgeInvocationSampleData.principal
    };
    const path = "/associateConnect";
    const contextPromise = new Promise(resolve => {
      testContext.app.get(
        path,
        [
          testContext.addon.authenticateForge(),
          testContext.addon.associateConnect()
        ],
        async (req, res) => {
          res.sendStatus(200);
          resolve(req.context);
        }
      );
    });

    await makeAppRequest("GET", path, {
      authorization: `bearer ${testContext.jwt}`,
      "x-forge-oauth-system": appToken,
      "x-forge-oauth-user": userToken
    });

    const context = await contextPromise;

    // check DB value of ClientSetting and forge installation map
    const clientSetting =
      await testContext.addon.settings.getClientSettingsForForgeInstallation(
        helper.forgeInstallationId
      );
    expect(clientSetting.clientKey).toEqual(helper.installedPayload.clientKey);
    expect(clientSetting.installationId).toEqual(helper.forgeInstallationId);

    // connect context check
    expect(context.title).toEqual(testContext.addon.name);
    expect(context.addonKey).toEqual(clientSetting.key);
    expect(context.clientKey).toEqual(clientSetting.clientKey);
    expect(context.hostBaseUrl).toEqual(helper.productBaseUrl);
    expect(context.userAccountId).toEqual(forgeInvocationSampleData.principal);
    expect(context.http.constructor.name).toEqual("HostClient");

    // forge context check
    expect(context.forge).toMatchObject(expectedForgeContext);
  });

  it("Can make an FRC request as the app", async () => {});
  it("Can make an FRC request as the user", async () => {});
  it("Make a JWT request as the app", async () => {});
  it("Make a JWT request as the user", async () => {}); // Can possibly do without this one
});

describe("Forge remote requests without a backing Connect Installation", () => {
  const { startServer, stopServer, makeAppRequest, testContext } =
    createTestContext();
  beforeAll(() => startServer(false, forgeInvocationSampleData.app.id));
  afterAll(stopServer);
  it("Sets the FRC tokens and api base URL on the context", async () => {
    const appToken = "app-token";
    const userToken = "user-token";
    const expectedForgeContext = {
      tokens: { app: appToken, user: userToken },
      app: {
        id: forgeInvocationSampleData.app.id,
        apiBaseUrl: forgeInvocationSampleData.app.apiBaseUrl
      },
      context: {
        cloudId: forgeInvocationSampleData.context.cloudId,
        siteUrl: forgeInvocationSampleData.context.siteUrl
      },
      apiBaseUrl: forgeInvocationSampleData.app.apiBaseUrl,
      principal: forgeInvocationSampleData.principal
    };
    const path = "/authenticated";
    const contextPromise = new Promise(resolve => {
      testContext.app.get(
        path,
        testContext.addon.authenticateForge(),
        (req, res) => {
          res.sendStatus(200);
          resolve(req.context);
        }
      );
    });

    await makeAppRequest("GET", path, {
      authorization: `bearer ${testContext.jwt}`,
      "x-forge-oauth-system": appToken,
      "x-forge-oauth-user": userToken
    });
    expect((await contextPromise).forge).toMatchObject(expectedForgeContext);
  });
});

describe("Handling of missing configuration", () => {
  const { startServer, stopServer, testContext } = createTestContext();
  beforeAll(() => startServer(false, undefined));
  afterAll(stopServer);
  it("Throws during Forge remote authentication setup when no appId set", async () => {
    let errorMessage = "No error thrown";
    try {
      testContext.addon.authenticateForge();
    } catch (e) {
      errorMessage = e.message;
    }
    expect(errorMessage).toContain("app ID must be configured");
  });
});

describe("Handling of invalid Forge remote requests", () => {
  const { startServer, stopServer, makeAppRequest, testContext } =
    createTestContext();
  const path = "/authenticated";
  beforeEach(async () => {
    await startServer(false, forgeInvocationSampleData.app.id);
    testContext.app.get(
      path,
      testContext.addon.authenticateForge(),
      (req, res) => {
        res.end();
      }
    );
  });

  afterAll(stopServer);

  it("Rejects a request with no FIT to an FRC-authenticated endpoint", async () => {
    const result = await makeAppRequest("GET", path);
    expect(result.response.statusCode).toEqual(401);
    expect(result.response.body).toEqual(
      "Forge Invocation Token verification was unsuccessful"
    );
  });

  it("Rejects a request with an invalid FIT to an FRC-authenticated endpoint", async () => {
    const result = await makeAppRequest("GET", path, {
      authorization: `Bearer ${testContext.jwt}` + "randomString"
    });
    expect(result.response.statusCode).toEqual(401);
    expect(result.response.body).toEqual(
      "Forge Invocation Token verification was unsuccessful"
    );
  });

  it("Rejects a request with a Forge Invocation Token for the wrong audience", async () => {
    await startServer(
      false,
      "ari:cloud:ecosystem::app/deadbeef-cafe-cafe-cafe-deadbeefdeed"
    );
    testContext.app.get(
      path,
      testContext.addon.authenticateForge(),
      (req, res) => {
        res.end();
      }
    );
    const result = await makeAppRequest("GET", path, {
      authorization: `Bearer ${testContext.jwt}`
    });
    expect(result.response.statusCode).toEqual(401);
    expect(result.response.body).toEqual(
      "Forge Invocation Token verification was unsuccessful"
    );
  });
});
