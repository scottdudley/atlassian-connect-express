const utils = require("../lib/internal/utils");

describe("sandBoxBaseUrl", () => {
  it("should return true when baseURL ends with 'atlassian-fex.net'", () => {
    expect(utils.isSandboxBaseUrl("http://foobar.atlassian-fex.net")).toBe(
      true
    );
  });

  it("should return true when baseURL has greater than 2 dots and ends with 'atlassian-fex.net'", () => {
    expect(utils.isSandboxBaseUrl("http://foo.bar.atlassian-fex.net")).toBe(
      true
    );
  });

  it("should return false when baseURL doesn't end with 'atlassian-fex.net'", () => {
    expect(utils.isSandboxBaseUrl("http://foobar.atlassian-fex.com")).toBe(
      false
    );
  });
});
